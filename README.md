Set up using these commands:

    $ virtualenv venv
    $ source ./venv/bin/activate
    $ pip install -r requirements.txt

    $ python manage.py syncdb
    $ python manage.py runserver

Open this URL: http://localhost:8000/saml2/login/