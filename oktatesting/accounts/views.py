from django.shortcuts import render
from django.http import HttpResponse


def index(request):
    return HttpResponse("Hello, world. You're at the accounts index.")


def profile(request):
    rv = u"""
<pre>
Username: {}
First name: {}
Last name: {}
Email address: {}
</pre>
    """.format(request.user.username,
               request.user.first_name,
               request.user.last_name,
               request.user.email)
    return HttpResponse(rv)
